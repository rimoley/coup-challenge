package com.rubencarpin.challenge.cli.service

import com.rubencarpin.challenge.cli.model.FleetStatus
import spock.lang.Specification
import spock.lang.Subject

class EngineerPickerTests extends Specification {

    @Subject
    EngineersPicker picker = new EngineersPicker();

    def "example test number 1 from challenge"() {
        given:
            def fleet = new FleetStatus(numberScootersByEngineers: 5, numberScootersOfManager: 12, districtsDistribution: [10, 15])
        when:
            def result = picker.howManyengineersINeedWith fleet
        then:
            result == 3
    }

    def "example test number 2 from challenge"() {
        given:
        def fleet = new FleetStatus(numberScootersByEngineers: 5, numberScootersOfManager: 9, districtsDistribution: [11, 15, 13])
        when:
        def result = picker.howManyengineersINeedWith fleet
        then:
        result == 7
    }

    def "lots of districts"() {
        given:
            def fleet = new FleetStatus(numberScootersByEngineers: 7, numberScootersOfManager: 10, districtsDistribution: [11, 15, 13, 3, 6, 8, 9, 12, 20, 10, 10, 10])
        when:
            def result = picker.howManyengineersINeedWith fleet
        then:
            result == 22
    }

    def "lazy manager and lazy engineers"() {
        given:
            def fleet = new FleetStatus(numberScootersByEngineers: 1, numberScootersOfManager: 1, districtsDistribution: [11, 15, 13, 3, 6, 8, 9, 12, 20, 10, 10, 10])
        when:
            def result = picker.howManyengineersINeedWith fleet
        then:
            result == 126
    }

    def "super hero manager and engineer"() {
        given:
            def fleet = new FleetStatus(numberScootersByEngineers: 100, numberScootersOfManager: 100, districtsDistribution: [11, 15, 13, 3, 6, 8, 9, 12, 20, 10, 10, 10])
        when:
            def result = picker.howManyengineersINeedWith fleet
        then:
            result == 11
    }

    def "no need of engineers"() {
        given:
        def fleet = new FleetStatus(numberScootersByEngineers: 100, numberScootersOfManager: 100, districtsDistribution: [11])
        when:
        def result = picker.howManyengineersINeedWith fleet
        then:
        result == 0
    }
}
