package com.rubencarpin.challenge.cli

import com.rubencarpin.challenge.cli.model.FleetStatus
import com.rubencarpin.challenge.cli.parser.FleetParser
import spock.lang.Specification
import spock.lang.Subject

class FleetParserTests extends Specification {

    @Subject
    FleetParser parser = new FleetParser();

    def "valid input gives a valid fleet status"() {
        given:
            def p = "5"
            def c = "10"
            def scooters = pathOf("valid_scooters.txt")
            def expectedStatus = new FleetStatus(numberScootersByEngineers: 5, numberScootersOfManager: 10, districtsDistribution: [10, 15])
        when:
            def result = parser.parse(p, c, scooters)
        then:
            result == expectedStatus
    }

    def "p input is not a number"() {
        given:
            def p = "a"
            def c = "10"
            def scooters = pathOf("valid_scooters.txt")
        when:
            def result = parser.parse(p, c, scooters)
        then:
            AssertionError ex = thrown()
            ex.message.contains('p must be a number')
    }

    def "p input is within range"() {
        given:
            def p = "1001"
            def c = "10"
            def scooters = pathOf("valid_scooters.txt")
        when:
            def result = parser.parse(p, c, scooters)
        then:
            AssertionError ex = thrown()
            ex.message.contains('p number must be between [1, 1000]')
    }
    def "c input is not a number"() {
        given:
            def p = "5"
            def c = "1-"
            def scooters = pathOf("valid_scooters.txt")
        when:
            def result = parser.parse(p, c, scooters)
        then:
            AssertionError ex = thrown()
            ex.message.contains('c must be a number')
    }

    def "c input is within range"() {
        given:
            def p = "10"
            def c = "0"
            def scooters = pathOf("valid_scooters.txt")
        when:
            def result = parser.parse(p, c, scooters)
        then:
            AssertionError ex = thrown()
            ex.message.contains('c number must be between [1, 999]')
    }

    def "file input does not exists"() {
        given:
            def p = "5"
            def c = "10"
            def scooters = "valid_scooters.txt"
        when:
            def result = parser.parse(p, c, scooters)
        then:
            AssertionError ex = thrown()
            ex.message.contains('Scooters distribution districts file must exists')
    }

    def "file input cannot be processed with non numeric characters"() {
        given:
            def p = "5"
            def c = "10"
            def scooters = pathOf("not_all_valid_scooters.txt")
        when:
            def result = parser.parse(p, c, scooters)
        then:
            AssertionError ex = thrown()
            ex.message.contains('All scooters fleet sizes must be numbers')
    }

    def "flle must not be empty"() {
        given:
            def p = "5"
            def c = "10"
            def scooters = pathOf("empty.txt")
        when:
            def result = parser.parse(p, c, scooters)
        then:
            AssertionError ex = thrown()
            ex.message.contains('Scooters file cannot be empty')
    }

    def pathOf(resource) {
        def path = FleetParserTests.getResource("/$resource").toURI().path
        path
    }

}
