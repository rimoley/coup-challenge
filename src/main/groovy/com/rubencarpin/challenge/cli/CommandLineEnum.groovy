package com.rubencarpin.challenge.cli

import com.rubencarpin.challenge.cli.service.EngineersPicker
import com.rubencarpin.challenge.cli.parser.FleetParser

enum CommandLineEnum {

    CLI

    CliBuilder commandLine

    CommandLineEnum() {
        commandLine = new CliBuilder(usage: helpMessage, header: "COUP CHALLENGE")
        commandLine.help("Print this message")
        commandLine.p(required: true, args: 1, 'Number of scooters per Fleet Engineer')
        commandLine.c(required: true, args: 1, "Number of scooters for the Fleet Manager")
        commandLine._(required: true, longOpt: "scootersFile", args:1, "File with scooters distrbution per district. File with one number per line")
    }

    void run(args) {
        OptionAccessor options = commandLine.parse(args)

        options || exitWithoutMessage()
        if (options.help) onlyUsage
        def fleetParser = new FleetParser()
        try {
            def fleetStatus = fleetParser.parse(options.p, options.c, options.scootersFile)
            def picker = new EngineersPicker()
            def noOfEngineers = picker.howManyengineersINeedWith fleetStatus
            println "Number of fleet engineers needed are $noOfEngineers"
        }
        catch(e) {
            exitWithMessage("Error in input data")
        }
    }

    def getHelpMessage() {
        """

fleet -p <int> -c <int> -s <path/to/file>

Example: fleet -p 10 -c 5 -s scooters.txt

"""
    }

    def getOnlyUsage() {
        commandLine.usage()
        System.exit(0)
    }

    def exitWithMessage(message) {
        println(message)
        commandLine.usage()
        System.exit(1)
    }

    def exitWithoutMessage() {
        System.exit(1)
    }
}