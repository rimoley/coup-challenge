package com.rubencarpin.challenge.cli.parser

import com.rubencarpin.challenge.cli.model.FleetStatus
import groovy.transform.CompileStatic

@CompileStatic
class FleetParser {

    def parse(String p, String c, String scootersFile) {
        [
            numberScootersByEngineers   : validateNumberScootersPerEngineers(p),
            numberScootersOfManager     : validateNumberScootersForManager(c),
            districtsDistribution       : validateScooters(scootersFile)
        ] as FleetStatus
    }

    def validateScooters(String scootersFile) {
        def file = new File(scootersFile)
        assert file.exists() && file.isFile(), "Scooters distribution districts file must exists"
        def content = file.text.trim()
        assert !content.isEmpty(), "Scooters file cannot be empty"
        def districts = content.split("\n")
        assert districts.size() > 0 && districts.size() <= 100, "The number of districts (file lines) should be between [1, 100]"
        assert districts.every { String element -> element.isNumber() }, "All scooters fleet sizes must be numbers"
        def districtsScooters = districts.collect {String scooterString -> scooterString.toLong() }
        assert districtsScooters.every { Long scootersSize -> scootersSize >= 0 && scootersSize <= 1000 }, "Scooters per districts must be between [0, 1000]"
        districtsScooters
    }

    def validateNumberScootersForManager(String c) {
        assert c.isNumber(), "c must be a number"
        def value = c.toLong()
        assert value >= 1 && value < 1000, "c number must be between [1, 999]"
        value
    }

    def validateNumberScootersPerEngineers(String p) {
        assert p.isNumber(), "p must be a number"
        def value = p.toLong()
        assert value >= 1 && value <= 1000, "p number must be between [1, 1000]"
        value
    }
}
