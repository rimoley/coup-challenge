package com.rubencarpin.challenge.cli

import groovy.transform.CompileStatic

@CompileStatic
class CommandLineEntryPoint {

    static main(args) {
        CommandLineEnum cli = CommandLineEnum.CLI
        cli.run(args)
    }
}
