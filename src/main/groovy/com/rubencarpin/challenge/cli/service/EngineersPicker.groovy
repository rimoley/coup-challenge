package com.rubencarpin.challenge.cli.service

import com.rubencarpin.challenge.cli.model.FleetStatus
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j

@CompileStatic
@Slf4j
class EngineersPicker {

    def howManyengineersINeedWith = { FleetStatus fleetStatus ->
        log.info("Calculating number of engineers for fleet status ${fleetStatus}")
        List<Long> sortedFleet = fleetStatus.districtsDistribution.toSorted()
        Long selectedDistrictFleet = sortedFleet.find { Long districtSize -> districtSize >= fleetStatus.numberScootersOfManager } ?: sortedFleet[-1]
        log.debug("Selected district fleet size is $selectedDistrictFleet")
        sortedFleet.removeAt(sortedFleet.findIndexOf { it  == selectedDistrictFleet } )
        Closure howManyEngineersFitInCurried = howManyEngineersFitIn.curry(fleetStatus.numberScootersByEngineers)
        Long restOfDistrictsCount = sortedFleet.collect(howManyEngineersFitInCurried).sum(0) as Long
        Long remainingFromSelectedDistrict = howManyEngineersFitInCurried(selectedDistrictFleet - fleetStatus.numberScootersOfManager) as Long
        log.info("Total engineers needed are ${restOfDistrictsCount + remainingFromSelectedDistrict}")
        restOfDistrictsCount + remainingFromSelectedDistrict
    }

    private Closure howManyEngineersFitIn = { Long perEngineer, Long districtFleet -> Math.ceil((double)districtFleet / perEngineer)}
}
