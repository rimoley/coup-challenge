package com.rubencarpin.challenge.cli.model

import groovy.transform.CompileStatic
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@CompileStatic
@EqualsAndHashCode
@ToString
class FleetStatus {

    Long numberScootersByEngineers
    Long numberScootersOfManager
    List<Long> districtsDistribution
}
