Coup Challenge
==============

How many fleet engineers do we need??

Given the amount of scooters a fleet manager can handle, and how many a fleet engineer can tackle, with a given districts fleet distribution this tool calculates the minimum amount of fleet engineers needed.

Project need Java 8 install to work.

Compile
-------

./gradlew clean classes

Test
----

./gradlew clean test

Bundle
------

./gradlew clean bundle

This will copy compiled code and dependencies with configuration and a run script to work with into 'build/distribution'

How this works?
---------------

'run.sh' file can be run with the following options
-p to define the number of scooters a Fleet manager can handle
-c to define the number of scooters a Fleet engineer can tackle
--scooters with the path to a file that contains a list of districts with scooter fleets. Each line is a district, in each line it is a number with the scooter fleet size

Have fun!
